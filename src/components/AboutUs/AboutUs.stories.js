import React from "react";
import documents from "./notes.md";
import AboutUs from ".";

export default {
  title: "AboutUs",
  component: AboutUs,
  parameters: {
    notes: documents,
  },
};

export const Component = () => <AboutUs />;
