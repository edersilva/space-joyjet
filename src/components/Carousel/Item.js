import React from "react";
import { Card } from "react-bootstrap";

const Item = (props) => {
  const { title, img } = props;
  return (
    <Card>
      <Card.Img variant="top" src={`./images/${img}`} />
      <Card.Body>
        <Card.Title>{title}</Card.Title>
        <Card.Text>
          Lorem ipsum dolor sit amet, consectetur adipiscing elit. Curabitur
          luctus aliquet sapien….
        </Card.Text>
      </Card.Body>
    </Card>
  );
};

export default Item;
