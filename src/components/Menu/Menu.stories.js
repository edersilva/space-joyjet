import React from "react";
import documents from "./notes.md";
import Menu from ".";

export default {
  title: "NavBar",
  component: Menu,
  parameters: {
    notes: documents,
    backgrounds: {
      default: 'blue',
      values: [
        { name: 'blue', value: '#4a90e2' },
        { name: 'black', value: '#000000' },
        { name: 'transparent', value: 'transparent' },
      ],
    },    
  },
};

export const Component = () => <Menu />;
