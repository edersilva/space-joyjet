# Component Button

This component will create a custom `Button`.

This component is highly customized. You can change background color, text and function.

This component accepts props:

  * **title:** Define the text of the `Button`

  * **type:** Define the type of the `Button`. Options:  `primary | warning | danger | success | error`. Default value: `primary`

  * **disable:** Enable or disable `button`. Default is: `enable`
  
  * **onClick:** Define a function

```javascript
import React from "react";
import PropTypes from "prop-types";
import "./Button.scss";

const Button = (props) => {
  const { type, label, disabled, onClick } = props;

  return (
    <button
      className={`btn btn-${type}`}
      disabled={disabled}
      onClick={onClick}
    >
      {label ? label : 'click'}
    </button>
  );
};

Button.propTypes = {
  /**
   Options:
  */
  type: PropTypes.oneOf(["primary", "warning", "danger", "success", "info"])
    .isRequired,
  /**
  Define the text of the button.
  */
  label: PropTypes.string,
  /**
  Disable or enable button.
  */
  disabled: PropTypes.bool,
  /**
  Define function of the button.
  */
  onClick: PropTypes.func
};

export default Button;
```