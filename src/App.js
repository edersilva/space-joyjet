import './App.scss';
import Header from './components/Header';
import CardSlides from './components/Carousel';
import AboutUs from './components/AboutUs';
import Footer from './components/Footer';

function App() {
  return (
    <>
    <Header />
    <CardSlides />
    <AboutUs />
    <Footer />
    </>
  );
}

export default App;
