import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import './Informations.scss'

const Informations = () => {
  return (
    <section>
      <Container className="d-sm-none d-md-block content informations" fluid>
        <div className="content">
          <Container>
            <Row className="align-items-md-center">
              <Col>
                <div className="content">
                  <strong className="title">
                    Trending <span>Today</span>
                  </strong>
                </div>
              </Col>
              <Col>
                <div className="content">
                  Lorem ipsum dolor sit amet, consectetuer adipiscing ligula
                  eget dolor.
                </div>
              </Col>
              <Col>
                <div className="content">
                  Lorem ipsum dolor sit amet, consectetuer adipiscing ligula
                  eget dolor.
                </div>
              </Col>
              <Col>
                <div className="content">
                  Lorem ipsum dolor sit amet, consectetuer adipiscing ligula
                  eget dolor.
                </div>
              </Col>
            </Row>
          </Container>
        </div>
      </Container>
    </section>
  );
};

export default Informations;
