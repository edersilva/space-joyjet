import React from "react";
import { object, text, boolean, select } from "@storybook/addon-knobs";
import documents from './notes.md';
import Button from ".";

export default {
  title: "Button",
  component: Button,
  parameters: {
    notes: documents,
  },  
};

export const TestButtonOptions = () => {
  return (
    <Button
      type={select("color", colorOptions, "primary")}
      onClick={object("onClick", onClickTest)}
      disabled={boolean("disabled", false)}
      label={text("label", "sucesso")}
    />
  );
};

const onClickTest = () => {
  console.log(documents);
  alert("Click here!");
};

const colorOptions = {
  primary: "primary",
  warning: "warning",
  danger: "danger",
  success: "success",
  info: "info",
};

export const Primary = () => (
  <Button type="primary" label="Primary" onClick="#" />
);
export const Warning = () => (
  <Button type="warning" label="Warning" onClick="#" />
);
export const Danger = () => (
  <Button type="danger" label="Danger" onClick="#" />
);
export const Success = () => (
  <Button type="success" label="Success" onClick="#" />
);
export const Info = () => <Button type="info" label="Info" onClick="#" />;
