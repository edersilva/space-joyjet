import React from "react";
import documents from "./notes.md";
import Informations from ".";

export default {
  title: "Informations",
  component: Informations,
  parameters: {
    notes: documents,
  },
};

export const Component = () => <Informations />;
