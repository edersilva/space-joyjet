import React from "react";
import documents from "./notes.md";
import Footer from ".";

export default {
  title: "Footer",
  component: Footer,
  parameters: {
    notes: documents,
  },
};

export const Component = () => <Footer />;
