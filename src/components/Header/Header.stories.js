import React from "react";
import documents from "./notes.md";
import Header from ".";

export default {
  title: "Header",
  component: Header,
  parameters: {
    notes: documents,
  },
};

export const Component = () => <Header />;
