import React from "react";
import documents from "./notes.md";
import CardSlides from ".";

export default {
  title: "Carousel",
  component: CardSlides,
  parameters: {
    notes: documents,
  },
};

export const Component = () => <CardSlides />;
