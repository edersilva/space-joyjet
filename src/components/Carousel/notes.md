# Component Carousel

This component will create a custom `Carousel`.

```javascript
import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import Slider from "react-slick";
import Item from "./Item";
import './Carousel.scss';
import './SlickSlider.scss';

const CardSlides = () => {
  var settings = {
    dots: false,
    infinite: false,
    speed: 500,
    slidesToShow: 3,
    slidesToScroll: 3,
    responsive: [
      {
        breakpoint: 1024,
        settings: {
          slidesToShow: 1,
          slidesToScroll: 1,
          infinite: true,
          dots: true,
          arrows: false,
        },
      },
    ],
  };
  return (
    <section className="carousel-items">
      <Container>
        <Row>
          <Col>
            <Slider {...settings}>
              <Item
                img="card1.jpg"
                title="International Space Station"
              />
              <Item
                img="card2.jpg"
                title="My capsule"
              />
              <Item
                img="card3.jpg"
                title="My Moon"
              />
              <Item
                img="card2.jpg"
                title="My capsule"
              />
              <Item
                img="card1.jpg"
                title="International Space Station"
              />              
            </Slider>
          </Col>
        </Row>
      </Container>
    </section>
  );
};

export default CardSlides;
```