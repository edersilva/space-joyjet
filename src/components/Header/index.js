import React from "react";
import { Container, Row, Col, Image } from "react-bootstrap";
import Menu from "../Menu/index";
import Informations from "../Informations";
import Button from "../Button";
import "./Header.scss";

const Header = () => {
  return (
    <>
      <header>
        <Menu />

        <Container className="content about">
          <Row>
            <Col>
              <h1>Space</h1>
              <p className="description">
                Lorem ipsum dolor sit amet, consectetur adipiscing elit. <br />
                Quisque molestie elit
              </p>
              <Button type="primary" text="click" url="#" />
            </Col>
          </Row>
        </Container>
        <Informations />
        <div className="bg"></div>
        <Image className="img-bg" src="./images/bg-header.jpg" />
      </header>
    </>
  );
};

export default Header;
