# Component Menu

This creates a component called `Menu`.

```javascript
import React from "react";
import { Container, Nav, Image } from "react-bootstrap";
import Navbar from "react-bootstrap/Navbar";
import './Menu.scss'

document.addEventListener("DOMContentLoaded", function(){
  window.addEventListener('scroll', function() {
      if (window.scrollY > 95) {
        document.getElementById('navbar_top').classList.add('fixed-top');
        const navbar_height = document.querySelector('.navbar').offsetHeight;
        document.body.style.paddingTop = navbar_height + 'px';
      } else {
        document.getElementById('navbar_top').classList.remove('fixed-top');
        document.body.style.paddingTop = '0';
      } 
  });
});

const Menu = () => {
  return (
    <Navbar id="navbar_top" className="content" collapseOnSelect expand="lg">
      <Container className="justify-content-md-between menu-mobile">
        <Navbar.Brand href="#home"><Image title="Space" src="./images/logo-space.png" /></Navbar.Brand>
        <Navbar.Toggle aria-controls="responsive-navbar-nav" />
        <Navbar.Collapse id="responsive-navbar-nav">
          <Nav className="me-auto">
            <Nav.Link href="#">Blog</Nav.Link>
            <Nav.Link href="#">Popular</Nav.Link>
            <Nav.Link href="#">Archive</Nav.Link>
            <Nav.Link href="#">About</Nav.Link>
          </Nav>
        </Navbar.Collapse>
      </Container>
    </Navbar>
  );
};

export default Menu;
```