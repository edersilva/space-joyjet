# Component Footer

This creates a component called `Footer`.

```javascript
import React from "react";
import { Container, Row, Col } from "react-bootstrap";
import './Footer.scss';

const Footer = () => {
  return (
    <footer className="footer">
      <Container>
        <Row>
          <Col>
            <div className="content">
              <p>© 2021 Created by Joyjet Digital Space Agency</p>
            </div>
          </Col>
        </Row>
      </Container>
    </footer>
  );
};

export default Footer;
```