![alt Joyjet](http://cdn.joyjet.com/email/signature_201604/joyjet-logo.gif)

# Joyjet Tech Interview

This project was create using [React JS](https://reactjs.org/) and [Storybook](https://storybook.js.org/).

### Project install:
After you cloned the project, enter the directory and run: 
`npm i`

### Run project:
In the project directory, you can run: 
`npm start`

### To show documentation:
In the project directory, you can run: 
`npm run storybook`